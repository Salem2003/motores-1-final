using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeEnergia : MonoBehaviour
{
    public Image barraDeEnergia;
    public float cantidadActualEnergia;
    public float cantidadMaxEnergia;

    void Update()
    {
        barraDeEnergia.fillAmount = cantidadActualEnergia / cantidadMaxEnergia;
    }
}
