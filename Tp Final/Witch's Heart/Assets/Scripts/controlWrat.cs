using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlWrat : MonoBehaviour
{
    [SerializeField] private Rotador velocidadRotacion;
    [SerializeField] private ControlMovEnemigo velocidad;
    private int cont = 0;
    private bool resetCont = false;

    void Update()
    {
            StartCoroutine(CoolDown(5));

        if (resetCont)
        {
            cont = 0;
            resetCont = false;
        }
    }

    private IEnumerator CoolDown(float valorCoolDown)
    {
        while (valorCoolDown > 0)
        {
            yield return new WaitForSeconds(1.0f);
            valorCoolDown--;
        }

        if (cont == 0)
        {
            velocidadRotacion.velocidadRotacionY = 2000;
            velocidad.rapidez = 12;
            cont = 1;
        }
        else if (cont == 1)
        {
            velocidadRotacion.velocidadRotacionY = 200;
            velocidad.rapidez = 5;
            resetCont = true;
        }

        StopAllCoroutines();
    }
}
