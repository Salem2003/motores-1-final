using System;
using UnityEngine;

public class GestorAudio : MonoBehaviour
{
    public Sonido[] sonidos;
    public static GestorAudio instancia; //�nica instancia con un �nico punto global de acceso (Patr�n Singleton)

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject); //No se destruye al objeto si se cambia de escena

        foreach (Sonido s in sonidos)
        {
            s.fuenteAudio = gameObject.AddComponent<AudioSource>(); //Agrega un componente por c�digo
            s.fuenteAudio.clip = s.clipSonido;
            s.fuenteAudio.volume = s.volume;
            s.fuenteAudio.pitch = s.pitch;
            s.fuenteAudio.loop = s.loop;
        }
    }

    public void ReproducirSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sound => sound.nombre == nombre);

        if (s == null)
        {
            Debug.LogWarning("Sonido " + nombre + " no encontrado");
            return;
        }
        else
        {
            s.fuenteAudio.Play();
        }
    }

    public void PausarSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sound => sound.nombre == nombre);

        if (s == null)
        {
            Debug.LogWarning("Sonido " + nombre + " no encontrado");
            return;
        }
        else
        {
            s.fuenteAudio.Pause();
        }
    }
}
