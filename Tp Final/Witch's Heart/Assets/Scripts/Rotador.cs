using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Rotador : MonoBehaviour
{
    [SerializeField] private int velocidadRotacionX;
    public int velocidadRotacionY;
    [SerializeField] private int velocidadRotacionZ;

    void Update()
    {
        transform.Rotate(new Vector3(velocidadRotacionX, velocidadRotacionY, velocidadRotacionZ) * Time.deltaTime);
    }
}