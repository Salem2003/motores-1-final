using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rebotador : MonoBehaviour
{
    private bool tengoQBajar;
    private int rapidezRebote = 10;

    void Start()
    {

    }

    void Update()
    {
        if (transform.position.y >= 8)
        {
            tengoQBajar = true;
        }
        if (transform.position.y <= 2)
        {
            tengoQBajar = false;
        }
        if (tengoQBajar)
        {
            Bajar();
        }
        else
        {
            Subir();
        }
    }

    void Subir()
    {
        transform.position += transform.up * rapidezRebote * Time.deltaTime;
    }

    void Bajar()
    {
        transform.position -= transform.up * rapidezRebote * Time.deltaTime;
    }
}
