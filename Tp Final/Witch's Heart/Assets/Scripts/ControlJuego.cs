using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ControlJuego : MonoBehaviour
{
    public GameObject jugador;
    public ControlJugador HP;
    public List<GameObject> listaEnemigos;
    public static ControlJuego instancia;
    private float tiempoCooldownSpawn;
    private float tiempoIntermedio = 20;

    [SerializeField] private Material skyboxNivel2;
    [SerializeField] private Material skyboxNivel3;

    public int numEnemigo;
    public int indiceNivel;
    private int indiceNivelIntermedio = 7;
    private int contEnemigos;
    private int contEnemigosAux;
    public int contTotalEnemigos;
    public bool jefe = false;
    public bool noEntrar = false;
    private bool spawn = false;
    private bool final = false;
    public bool reinicio = false;
    public bool guardarVida = false;

    [SerializeField] private TMP_Text txtCantEnemigos;
    [SerializeField] private TMP_Text txtHPJugador;
    [SerializeField] private TMP_Text txtColorPro;
    [SerializeField] private TMP_Text txtCooldownUlti;
    [SerializeField] private TMP_Text txtEnergia;
    [SerializeField] private TMP_Text txtFin;

    [SerializeField] private GameObject Mapa1;
    [SerializeField] private GameObject Mapa2;
    [SerializeField] private GameObject Mapa3;
    [SerializeField] private GameObject Mapa4;
    [SerializeField] private GameObject Mapa5;
    [SerializeField] private GameObject Mapa6;
    [SerializeField] private GameObject MapaIntermedio;

    public GameObject Aran;
    public GameObject Welluw;
    [SerializeField] private GameObject Beukelaar;
    [SerializeField] private GameObject Helm;
    [SerializeField] private GameObject Geest;
    [SerializeField] private GameObject Tand;
    [SerializeField] private GameObject Inktvis;
    [SerializeField] private GameObject Bloem;
    [SerializeField] private GameObject Ruw;
    [SerializeField] private GameObject Spellen;
    [SerializeField] private GameObject Spin;
    [SerializeField] private GameObject Engel;

    [SerializeField] private GameObject Wrat;
    [SerializeField] private GameObject Vis;
    [SerializeField] private GameObject Seraf;
    [SerializeField] private GameObject Behoren;
    [SerializeField] private GameObject Koning;
    [SerializeField] private GameObject Heks;

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        //indiceNivel = 1;
        contTotalEnemigos = -1;
        SetearTextos();
        ComenzarNivel(indiceNivel);
    }

    public void SetearTextos()
    {
        txtHPJugador.text = $"{HP.HP}/{HP.HPAux}";
        txtCantEnemigos.text = string.Empty;
        txtCooldownUlti.text = $"(Q) Cooldown Ulti: {HP.cooldownUlti}";
        txtEnergia.text = $"(E) Energia: {HP.energia}";
        txtFin.text = string.Empty;

        if (!jefe)
        {
            txtCantEnemigos.text = $"Enemigos restantes: {contTotalEnemigos}";
        }
        
        if (jefe || contTotalEnemigos < 0)
        {
            txtCantEnemigos.text = string.Empty;
        }

        if (indiceNivel > 6 && tiempoIntermedio < 20)
        {
            txtFin.text = "FIN";
        }
    }

    public void SetearTextos(string colorPro)
    {
        txtColorPro.text = $"(1-6) Color: {colorPro}";
    }

    void Update()
    {
        SetearTextos();

        if (contTotalEnemigos == 0 && !jefe && !final)
        {
            jefe = true;
            SpawnearEnemigos(indiceNivel);
        }

        if (contTotalEnemigos <= 0 && jefe)
        {
            HP.desactivarCorutinas = false;
            jefe = false;
            spawn = false;
            HP.instanciaJefe = false;
            contTotalEnemigos = -1;
            indiceNivel += 1;
            //Debug.Log(indiceNivel);
            StartCoroutine(EsperarPasajeDeNivel(5));
        }

        if (tiempoIntermedio == 0 && !final)
        {
            SetearTextos();

            noEntrar = true;

            tiempoIntermedio = 20;
            ComenzarNivel(indiceNivel);
        }

        if (Input.GetKeyDown(KeyCode.R) /*&& final && contTotalEnemigos <= 0*/)
        {
            indiceNivel = 1;
            reinicio = true;
            HP.energia = 0;
            HP.cooldownUlti = HP.ultiAux;
            ReiniciarNivel(indiceNivel);
        }
    }

    public void ComenzarNivel(int nivel)
    {
        StopAllCoroutines();

        if (nivel == 1)
        {
            jugador.transform.position = new Vector3(0f, 7f, 0f);
            guardarVida = true;
        }
        else if (nivel == 2)
        {
            jugador.transform.position = new Vector3(6903f, 7f, 2084f);
            guardarVida = true;
        }
        else if (nivel == 3)
        {
            jugador.transform.position = new Vector3(2586f, 7f, -1647f);
            guardarVida = true;
        }
        else if (nivel == 4)
        {
            jugador.transform.position = new Vector3(-3067f, 7f, -1647f);
            guardarVida = true;
        }
        else if (nivel == 5)
        {
            jugador.transform.position = new Vector3(254f, 7f, -2950f);
            guardarVida = true;
        }
        else if (nivel == 6)
        {
            jugador.transform.position = new Vector3(2516f, 7f, -2950f);
            guardarVida = true;
        }
        else if (nivel == 7)
        {
            jugador.transform.position = new Vector3(2507f, 7f, -4211f);
            guardarVida = false;

            StartCoroutine(EsperaNivelIntermedio(tiempoIntermedio));
        }

        ActivarMapa(nivel);

        foreach (GameObject item in listaEnemigos)
        {
            Destroy(item);
        }

        noEntrar = true;

        if (!spawn)
        {
            SpawnearEnemigos(nivel);
        }

        ReproducirAudio(nivel);
    }

    private IEnumerator EsperarPasajeDeNivel(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo--;
        }

        ComenzarNivel(indiceNivelIntermedio);
    }

    private IEnumerator EsperaNivelIntermedio(float tiempo)
    {
        while (tiempoIntermedio > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoIntermedio--;
        }
    }

    private IEnumerator CoolDownSpawnNivel1(float espera)
    {
        tiempoCooldownSpawn = espera;
        while (tiempoCooldownSpawn > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCooldownSpawn--;
        }

        if (tiempoCooldownSpawn == 0)
        {
            if (contTotalEnemigos != 0 && noEntrar)
            {
                contEnemigos = 0;
                noEntrar = false;
            }
            if ((contEnemigosAux + 1) >= contEnemigos)
            {
                if ((contEnemigos >= 0 && contEnemigos <= 4) || (contEnemigos >= 6 && contEnemigos <= 8) || (contEnemigos == 10) || (contEnemigos >= 14 && contEnemigos <= 17) || (contEnemigos == 21))
                {
                    numEnemigo = 1;
                    listaEnemigos.Add(Instantiate(Aran, new Vector3(Random.Range(-103, 100), 5.57f, Random.Range(-105, 97)), Quaternion.identity));
                }
                if ((contEnemigos == 5) || (contEnemigos == 13) || (contEnemigos >= 18 && contEnemigos <= 19))
                {
                    numEnemigo = 6;
                    listaEnemigos.Add(Instantiate(Geest, new Vector3(Random.Range(-103, 100), 5.53f, Random.Range(-105, 97)), Quaternion.identity));
                }
                if ((contEnemigos == 9) || (contEnemigos == 11) || (contEnemigos >= 22 && contEnemigos <= 23))
                {
                    numEnemigo = 3;
                    listaEnemigos.Add(Instantiate(Beukelaar, new Vector3(Random.Range(-103, 100), 6.57f, Random.Range(-105, 97)), Quaternion.identity));
                }
                if (contEnemigos == 25)
                {
                    numEnemigo = 11;
                    listaEnemigos.Add(Instantiate(Ruw, new Vector3(Random.Range(-103, 100), 6.12f, Random.Range(-105, 97)), Quaternion.identity));
                }
                if ((contEnemigos == 12) || (contEnemigos == 20) || (contEnemigos == 24))
                {
                    numEnemigo = 8;
                    listaEnemigos.Add(Instantiate(Helm, new Vector3(Random.Range(-103, 100), 10.5f, Random.Range(-105, 97)), Quaternion.identity));
                }
                if ((contEnemigos == 26))
                {
                    StopAllCoroutines();
                    spawn = true;
                }
                /*if (contTotalEnemigos <= 0 && jefe)
                {
                    contTotalEnemigos = 4;
                    SetearTextos();
                    numEnemigo = 18;
                    ReproducirSonidoSpawn(numEnemigo);
                    ReproducirAudio(indiceNivel);
                    listaEnemigos.Add(Instantiate(Wrat, new Vector3(Random.Range(-103, 100), 5f, Random.Range(-105, 97)), Quaternion.identity));
                    listaEnemigos.Add(Instantiate(Wrat, new Vector3(Random.Range(-103, 100), 5f, Random.Range(-105, 97)), Quaternion.identity));
                    StopAllCoroutines();
                }*/
            }

            contEnemigos++;
            ReproducirSonidoSpawn(numEnemigo);

            if (!spawn)
            {
                StartCoroutine(CoolDownSpawnNivel1(3));
            }
        }
    }

    private IEnumerator CoolDownSpawnNivel2(float espera)
    {

        tiempoCooldownSpawn = espera;
        while (tiempoCooldownSpawn > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCooldownSpawn--;
        }

        if (tiempoCooldownSpawn == 0)
        {
            if (contTotalEnemigos != 0 && noEntrar)
            {
                contEnemigos = 0;
                noEntrar = false;
            }
            if ((contEnemigosAux + 1) >= contEnemigos)
            {
                if ((contEnemigos >= 0 && contEnemigos <= 3) || (contEnemigos >= 6 && contEnemigos <= 8) || (contEnemigos >= 14 && contEnemigos <= 19))
                {
                    numEnemigo = 1;
                    listaEnemigos.Add(Instantiate(Aran, new Vector3(Random.Range(6781, 7000), 5.57f, Random.Range(1957, 2196)), Quaternion.identity));
                }
                if ((contEnemigos >= 4 && contEnemigos <= 5) || (contEnemigos == 10) || (contEnemigos == 24) || (contEnemigos >= 27 && contEnemigos <= 29) || (contEnemigos == 32))
                {
                    numEnemigo = 6;
                    listaEnemigos.Add(Instantiate(Geest, new Vector3(Random.Range(6781, 7000), 5.53f, Random.Range(1957, 2196)), Quaternion.identity));
                }
                if ((contEnemigos == 9) || (contEnemigos >= 11 && contEnemigos <= 13) || (contEnemigos >= 21 && contEnemigos <= 23))
                {
                    numEnemigo = 3;
                    listaEnemigos.Add(Instantiate(Beukelaar, new Vector3(Random.Range(6781, 7000), 6.57f, Random.Range(1957, 2196)), Quaternion.identity));
                }
                if ((contEnemigos == 20) || (contEnemigos == 30) || (contEnemigos >= 37 && contEnemigos <= 39))
                {
                    numEnemigo = 11;
                    listaEnemigos.Add(Instantiate(Ruw, new Vector3(Random.Range(6781, 7000), 6.12f, Random.Range(1957, 2196)), Quaternion.identity));
                }
                if ((contEnemigos >= 25 && contEnemigos <= 26) || (contEnemigos == 31) || (contEnemigos >= 34 && contEnemigos <= 35))
                {
                    numEnemigo = 9;
                    listaEnemigos.Add(Instantiate(Inktvis, new Vector3(Random.Range(6781, 7000), 6.45f, Random.Range(1957, 2196)), Quaternion.identity));
                }
                if ((contEnemigos == 33) || (contEnemigos == 36))
                {
                    numEnemigo = 13;
                    listaEnemigos.Add(Instantiate(Spellen, new Vector3(Random.Range(6781, 7000), 5.18f, Random.Range(1957, 2196)), Quaternion.identity));
                }
                if ((contEnemigos == 40))
                {
                    StopAllCoroutines();

                    spawn = true;
                }
            }

            contEnemigos++;
            ReproducirSonidoSpawn(numEnemigo);

            if (!spawn)
            {
                StartCoroutine(CoolDownSpawnNivel2(3));
            }
        }
    }

    private IEnumerator CoolDownSpawnNivel3(float espera)
    {
        tiempoCooldownSpawn = espera;
        while (tiempoCooldownSpawn > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCooldownSpawn--;
        }

        if (tiempoCooldownSpawn == 0)
        {
            if (contTotalEnemigos != 0 && noEntrar)
            {
                contEnemigos = 0;
                noEntrar = false;
            }
            if ((contEnemigosAux + 1) >= contEnemigos)
            {
                if ((contEnemigos >= 0 && contEnemigos <= 5) || (contEnemigos >= 12 && contEnemigos <= 15) || (contEnemigos >= 32 && contEnemigos <= 37) || (contEnemigos >= 48 && contEnemigos <= 50))
                {
                    numEnemigo = 1;
                    listaEnemigos.Add(Instantiate(Aran, new Vector3(Random.Range(2475, 2700), 5.57f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos >= 9 && contEnemigos <= 10) || (contEnemigos == 17) || (contEnemigos == 24) || (contEnemigos >= 39 && contEnemigos <= 40) || (contEnemigos == 45))
                {
                    numEnemigo = 6;
                    listaEnemigos.Add(Instantiate(Geest, new Vector3(Random.Range(2475, 2700), 5.53f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos == 11) || (contEnemigos == 16) || (contEnemigos >= 25 && contEnemigos <= 26) || (contEnemigos == 46))
                {
                    numEnemigo = 3;
                    listaEnemigos.Add(Instantiate(Beukelaar, new Vector3(Random.Range(2475, 2700), 6.57f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos >= 6 && contEnemigos <= 7) || (contEnemigos >= 22 && contEnemigos <= 23) || (contEnemigos == 38) || (contEnemigos == 44))
                {
                    numEnemigo = 8;
                    listaEnemigos.Add(Instantiate(Helm, new Vector3(Random.Range(2475, 2700), 10.5f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos >= 19 && contEnemigos <= 21) || (contEnemigos >= 27 && contEnemigos <= 29))
                {
                    numEnemigo = 9;
                    listaEnemigos.Add(Instantiate(Inktvis, new Vector3(Random.Range(2475, 2700), 6.45f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos == 8) || (contEnemigos == 18) || (contEnemigos == 30) || (contEnemigos == 41) || (contEnemigos == 51))
                {
                    numEnemigo = 13;
                    listaEnemigos.Add(Instantiate(Spellen, new Vector3(Random.Range(2475, 2700), 5.18f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos == 31) || (contEnemigos >= 42 && contEnemigos <= 43))
                {
                    numEnemigo = 17;
                    listaEnemigos.Add(Instantiate(Welluw, new Vector3(Random.Range(2475, 2700), 7.53f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos == 47) || (contEnemigos == 52))
                {
                    numEnemigo = 14;
                    listaEnemigos.Add(Instantiate(Spin, new Vector3(Random.Range(2475, 2700), 6.23f, Random.Range(-1769, -1543)), Quaternion.identity));
                }
                if ((contEnemigos == 53))
                {
                    StopAllCoroutines();

                    spawn = true;
                }
            }

            contEnemigos++;
            ReproducirSonidoSpawn(numEnemigo);

            if (!spawn)
            {
                StartCoroutine(CoolDownSpawnNivel3(4));
            }
        }
    }

    private IEnumerator CoolDownSpawnNivel4(float espera)
    {

        tiempoCooldownSpawn = espera;
        while (tiempoCooldownSpawn > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCooldownSpawn--;
        }

        if (tiempoCooldownSpawn == 0)
        {
            if (contTotalEnemigos != 0 && noEntrar)
            {
                contEnemigos = 0;
                noEntrar = false;
            }
            if ((contEnemigosAux - 1) >= contEnemigos)
            {
                if ((contEnemigos >= 0 && contEnemigos <= 4) || (contEnemigos >= 10 && contEnemigos <= 14) || (contEnemigos >= 19 && contEnemigos <= 22) || (contEnemigos >= 31 && contEnemigos <= 33) || (contEnemigos >= 44 && contEnemigos <= 47) || (contEnemigos >= 57 && contEnemigos <= 60))
                {
                    numEnemigo = 1;
                    listaEnemigos.Add(Instantiate(Aran, new Vector3(Random.Range(-3334, -2796), 5.57f, Random.Range(-1923, -1381)), Quaternion.identity));
                }
                if ((contEnemigos >= 5 && contEnemigos <= 7) || (contEnemigos >= 17 && contEnemigos <= 18) || (contEnemigos == 25) || (contEnemigos >= 34 && contEnemigos <= 35) || (contEnemigos >= 41 && contEnemigos <= 43) || (contEnemigos == 53))
                {
                    numEnemigo = 8;
                    listaEnemigos.Add(Instantiate(Helm, new Vector3(Random.Range(-3334, -2796), 10.5f, Random.Range(-1923, -1381)), Quaternion.identity));
                }
                if ((contEnemigos >= 28 && contEnemigos <= 30) || (contEnemigos >= 36 && contEnemigos <= 37) || (contEnemigos >= 49 && contEnemigos <= 52))
                {
                    numEnemigo = 9;
                    listaEnemigos.Add(Instantiate(Inktvis, new Vector3(Random.Range(-3334, -2796), 6.45f, Random.Range(-1923, -1381)), Quaternion.identity));
                }
                if ((contEnemigos >= 8 && contEnemigos <= 9) || (contEnemigos >= 23 && contEnemigos <= 24) || (contEnemigos == 38) || (contEnemigos >= 54 && contEnemigos <= 56))
                {
                    numEnemigo = 11;
                    listaEnemigos.Add(Instantiate(Ruw, new Vector3(Random.Range(-3334, -2796), 6.12f, Random.Range(-1923, -1381)), Quaternion.identity));
                }
                if ((contEnemigos >= 15 && contEnemigos <= 16) || (contEnemigos >= 26 && contEnemigos <= 27) || (contEnemigos >= 39 && contEnemigos <= 40) || (contEnemigos >= 61 && contEnemigos <= 62))
                {
                    numEnemigo = 17;
                    listaEnemigos.Add(Instantiate(Welluw, new Vector3(Random.Range(-3334, -2796), 7.53f, Random.Range(-1923, -1381)), Quaternion.identity));
                }
                if ((contEnemigos == 48) || (contEnemigos == 63))
                {
                    numEnemigo = 5;
                    listaEnemigos.Add(Instantiate(Engel, new Vector3(Random.Range(-3334, -2796), 31.1f, Random.Range(-1923, -1381)), Quaternion.identity));
                }
                if ((contEnemigos == 63))
                {
                    StopAllCoroutines();

                    spawn = true;
                }
            }

            contEnemigos++;
            ReproducirSonidoSpawn(numEnemigo);

            if (!spawn)
            {
                StartCoroutine(CoolDownSpawnNivel4(4));
            }
        }
    }

    private IEnumerator CoolDownSpawnNivel5(float espera)
    {
        tiempoCooldownSpawn = espera;
        while (tiempoCooldownSpawn > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCooldownSpawn--;
        }

        if (tiempoCooldownSpawn == 0)
        {
            if (contTotalEnemigos != 0 && noEntrar)
            {
                contEnemigos = 0;
                noEntrar = false;
            }
            if ((contEnemigosAux + 1) >= contEnemigos)
            {
                if ((contEnemigos >= 0 && contEnemigos <= 6) || (contEnemigos >= 10 && contEnemigos <= 13) || (contEnemigos >= 17 && contEnemigos <= 22) || (contEnemigos >= 26 && contEnemigos <= 29) ||
                    (contEnemigos >= 35 && contEnemigos <= 38) || (contEnemigos >= 55 && contEnemigos <= 59))
                {
                    numEnemigo = 1;
                    listaEnemigos.Add(Instantiate(Aran, new Vector3(Random.Range(78, 425), 5.57f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos == 16) || (contEnemigos == 30) || (contEnemigos == 47) || (contEnemigos == 60) || (contEnemigos >= 75 && contEnemigos <= 76))
                {
                    numEnemigo = 14;
                    listaEnemigos.Add(Instantiate(Spin, new Vector3(Random.Range(78, 425), 6.23f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos >= 31 && contEnemigos <= 33) || (contEnemigos >= 48 && contEnemigos <= 50) || (contEnemigos >= 71 && contEnemigos <= 73))
                {
                    numEnemigo = 13;
                    listaEnemigos.Add(Instantiate(Spellen, new Vector3(Random.Range(78, 425), 5.18f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos >= 7 && contEnemigos <= 9) || (contEnemigos >= 24 && contEnemigos <= 25) || (contEnemigos >= 44 && contEnemigos <= 46) || (contEnemigos >= 63 && contEnemigos <= 65))
                {
                    numEnemigo = 11;
                    listaEnemigos.Add(Instantiate(Ruw, new Vector3(Random.Range(78, 425), 6.12f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos >= 14 && contEnemigos <= 15) || (contEnemigos >= 41 && contEnemigos <= 43) || (contEnemigos >= 61 && contEnemigos <= 62) || (contEnemigos == 74))
                {
                    numEnemigo = 17;
                    listaEnemigos.Add(Instantiate(Welluw, new Vector3(Random.Range(78, 425), 7.53f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos == 51) || (contEnemigos == 70) || (contEnemigos == 80))
                {
                    numEnemigo = 5;
                    listaEnemigos.Add(Instantiate(Engel, new Vector3(Random.Range(78, 425), 31.1f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos == 23) || (contEnemigos == 34) || (contEnemigos == 54) || (contEnemigos == 66) || (contEnemigos == 77))
                {
                    numEnemigo = 15;
                    listaEnemigos.Add(Instantiate(Tand, new Vector3(Random.Range(78, 425), 5.57f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos >= 39 && contEnemigos <= 40) || (contEnemigos >= 52 && contEnemigos <= 53) || (contEnemigos >= 67 && contEnemigos <= 69) || (contEnemigos >= 78 && contEnemigos <= 79))
                {
                    numEnemigo = 4;
                    listaEnemigos.Add(Instantiate(Bloem, new Vector3(Random.Range(78, 425), 5.76f, Random.Range(-3410, -2786)), Quaternion.identity));
                }
                if ((contEnemigos == 81))
                {
                    StopAllCoroutines();

                    spawn = true;
                }
            }

            contEnemigos++;
            ReproducirSonidoSpawn(numEnemigo);

            if (!spawn)
            {
                StartCoroutine(CoolDownSpawnNivel5(5));
            }
        }
    }

    private IEnumerator CoolDownSpawnNivel6(float espera)
    {
        tiempoCooldownSpawn = espera;
        while (tiempoCooldownSpawn > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempoCooldownSpawn--;
        }

        if (tiempoCooldownSpawn == 0)
        {
            if (contTotalEnemigos != 0 && noEntrar)
            {
                contEnemigos = 0;
                noEntrar = false;
            }
            if ((contEnemigosAux - 1) >= contEnemigos)
            {
                if ((contEnemigos >= 0 && contEnemigos <= 7) || (contEnemigos >= 44 && contEnemigos <= 50))
                {
                    numEnemigo = 1;
                    listaEnemigos.Add(Instantiate(Aran, new Vector3(Random.Range(2370, 2642), 5.57f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 41 && contEnemigos <= 42) || (contEnemigos >= 67 && contEnemigos <= 70) || (contEnemigos >= 87 && contEnemigos <= 88))
                {
                    numEnemigo = 14;
                    listaEnemigos.Add(Instantiate(Spin, new Vector3(Random.Range(2370, 2642), 6.23f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 20 && contEnemigos <= 21) || (contEnemigos == 24) || (contEnemigos == 53) || (contEnemigos >= 92 && contEnemigos <= 93) || (contEnemigos >= 96 && contEnemigos <= 97))
                {
                    numEnemigo = 13;
                    listaEnemigos.Add(Instantiate(Spellen, new Vector3(Random.Range(2370, 2642), 5.18f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 16 && contEnemigos <= 18) || (contEnemigos >= 35 && contEnemigos <= 38) || (contEnemigos >= 60 && contEnemigos <= 63))
                {
                    numEnemigo = 11;
                    listaEnemigos.Add(Instantiate(Ruw, new Vector3(Random.Range(2370, 2642), 6.12f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos == 19) || (contEnemigos >= 64 && contEnemigos <= 65) || (contEnemigos >= 76 && contEnemigos <= 79))
                {
                    numEnemigo = 17;
                    listaEnemigos.Add(Instantiate(Welluw, new Vector3(Random.Range(2370, 2642), 7.53f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos == 32) || (contEnemigos == 43) || (contEnemigos >= 98 && contEnemigos <= 100) || (contEnemigos == 101))
                {
                    numEnemigo = 5;
                    listaEnemigos.Add(Instantiate(Engel, new Vector3(Random.Range(2370, 2642), 31.1f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos == 25) || (contEnemigos >= 58 && contEnemigos <= 59) || (contEnemigos >= 94 && contEnemigos <= 95))
                {
                    numEnemigo = 15;
                    listaEnemigos.Add(Instantiate(Tand, new Vector3(Random.Range(2370, 2642), 5.57f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 26 && contEnemigos <= 27) || (contEnemigos >= 39 && contEnemigos <= 40) || (contEnemigos >= 89 && contEnemigos <= 91))
                {
                    numEnemigo = 4;
                    listaEnemigos.Add(Instantiate(Bloem, new Vector3(Random.Range(2370, 2642), 5.76f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 8 && contEnemigos <= 10) || (contEnemigos >= 54 && contEnemigos <= 55) || (contEnemigos >= 73 && contEnemigos <= 75))
                {
                    numEnemigo = 3;
                    listaEnemigos.Add(Instantiate(Beukelaar, new Vector3(Random.Range(2370, 2642), 5.76f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 11 && contEnemigos <= 12) || (contEnemigos >= 33 && contEnemigos <= 34) || (contEnemigos == 66) || (contEnemigos >= 83 && contEnemigos <= 86))
                {
                    numEnemigo = 8;
                    listaEnemigos.Add(Instantiate(Helm, new Vector3(Random.Range(2370, 2642), 5.76f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 22 && contEnemigos <= 23) || (contEnemigos >= 51 && contEnemigos <= 52) || (contEnemigos >= 71 && contEnemigos <= 72))
                {
                    numEnemigo = 6;
                    listaEnemigos.Add(Instantiate(Geest, new Vector3(Random.Range(2370, 2642), 5.76f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos >= 13 && contEnemigos <= 15) || (contEnemigos >= 28 && contEnemigos <= 31) || (contEnemigos >= 56 && contEnemigos <= 57) || (contEnemigos >= 80 && contEnemigos <= 82))
                {
                    numEnemigo = 9;
                    listaEnemigos.Add(Instantiate(Inktvis, new Vector3(Random.Range(2370, 2642), 5.76f, Random.Range(-3094, -2826)), Quaternion.identity));
                }
                if ((contEnemigos == 101))
                {
                    StopAllCoroutines();

                    spawn = true;
                }
            }

            contEnemigos++;
            ReproducirSonidoSpawn(numEnemigo);

            if (!spawn)
            {
                StartCoroutine(CoolDownSpawnNivel6(5));
            }
        }
    }

    private void SpawnearEnemigos(int nivel)
    {
        guardarVida = false;

        if (nivel == 1)
        {
            contTotalEnemigos = 0;

            if (!jefe)
            {
                contTotalEnemigos = 26;
                contEnemigosAux = contTotalEnemigos;
                StartCoroutine(CoolDownSpawnNivel1(3));
            }

            if (jefe)
            {
                contTotalEnemigos = 2; //4
                SetearTextos();
                numEnemigo = 18;
                ReproducirSonidoSpawn(numEnemigo);
                ReproducirAudio(indiceNivel);
                listaEnemigos.Add(Instantiate(Wrat, new Vector3(Random.Range(-103, 100), 5f, Random.Range(-105, 97)), Quaternion.identity));
                //listaEnemigos.Add(Instantiate(Wrat, new Vector3(Random.Range(-103, 100), 5f, Random.Range(-105, 97)), Quaternion.identity));
            }
        }
        else if (nivel == 2)
        {
            contTotalEnemigos = 0;

            if (!jefe)
            {
                contTotalEnemigos = 40;
                contEnemigosAux = contTotalEnemigos;
                StartCoroutine(CoolDownSpawnNivel2(3));
            }
            if (jefe)
            {
                contTotalEnemigos = 2;
                SetearTextos();
                numEnemigo = 16;
                ReproducirSonidoSpawn(numEnemigo);
                ReproducirAudio(indiceNivel);
                listaEnemigos.Add(Instantiate(Vis, new Vector3(Random.Range(6781, 7000), 16.49f, Random.Range(1957, 2196)), Quaternion.identity));
            }
        }
        else if (nivel == 3)
        {
            contTotalEnemigos = 0;

            if (!jefe)
            {
                contTotalEnemigos = 53;
                contEnemigosAux = contTotalEnemigos;
                StartCoroutine(CoolDownSpawnNivel3(4));
            }
            if (jefe)
            {
                contTotalEnemigos = 2;
                SetearTextos();
                numEnemigo = 12;
                ReproducirSonidoSpawn(numEnemigo);
                ReproducirAudio(indiceNivel);
                listaEnemigos.Add(Instantiate(Seraf, new Vector3(Random.Range(2475, 2700), 7.72f, Random.Range(-1769, -1543)), Quaternion.identity));
            }
        }
        else if (nivel == 4)
        {
            contTotalEnemigos = 0;

            if (!jefe)
            {
                contTotalEnemigos = 64;
                contEnemigosAux = contTotalEnemigos;
                StartCoroutine(CoolDownSpawnNivel4(4));
            }
            if (jefe)
            {
                contTotalEnemigos = 2;
                SetearTextos();
                numEnemigo = 2;
                ReproducirSonidoSpawn(numEnemigo);
                ReproducirAudio(indiceNivel);
                listaEnemigos.Add(Instantiate(Behoren, new Vector3(Random.Range(-3334, -2796), 78.6f, Random.Range(-1923, -1381)), Quaternion.identity));
            }
        }
        else if (nivel == 5)
        {
            contTotalEnemigos = 0;

            if (!jefe)
            {
                contTotalEnemigos = 81;
                contEnemigosAux = contTotalEnemigos;
                StartCoroutine(CoolDownSpawnNivel5(5));
            }
            if (jefe)
            {
                contTotalEnemigos = 2;
                SetearTextos();
                numEnemigo = 10;
                ReproducirSonidoSpawn(numEnemigo);
                ReproducirAudio(indiceNivel);
                listaEnemigos.Add(Instantiate(Koning, new Vector3(Random.Range(78, 425), 18.74f, Random.Range(-3410, -2786)), Quaternion.identity));
            }
        }
        else if (nivel == 6)
        {
            contTotalEnemigos = 0;

            if (!jefe)
            {
                contTotalEnemigos = 101;
                contEnemigosAux = contTotalEnemigos;
                StartCoroutine(CoolDownSpawnNivel6(5));
            }
            if (jefe)
            {
                final = true;
                contTotalEnemigos = 1;
                SetearTextos();
                numEnemigo = 7;
                ReproducirSonidoSpawn(numEnemigo);
                ReproducirAudio(indiceNivel);
                listaEnemigos.Add(Instantiate(Heks, new Vector3(2511f, 32.4f, -2955f), Quaternion.identity));
            }
        }
    }

    private void ReproducirAudio(int nivel)
    {
        if (nivel == 1)
        {
            if (!jefe)
            {
                GestorAudio.instancia.PausarSonido("Tar Sea BGM");
                GestorAudio.instancia.PausarSonido("Dream Lab BGM");
                GestorAudio.instancia.PausarSonido("Defile BGM");
                GestorAudio.instancia.PausarSonido("Gold Castle BGM");
                GestorAudio.instancia.PausarSonido("She BGM");

                GestorAudio.instancia.PausarSonido("Wrat Boss Fight");
                GestorAudio.instancia.PausarSonido("Vis Boss Fight");
                GestorAudio.instancia.PausarSonido("Seraf Boss Fight");
                GestorAudio.instancia.PausarSonido("Behoren Boss Fight");
                GestorAudio.instancia.PausarSonido("Koning Boss Fight");
                GestorAudio.instancia.PausarSonido("Heks Boss Fight");

                GestorAudio.instancia.ReproducirSonido("Abyss BGM");
            }

            if (jefe)
            {
                GestorAudio.instancia.PausarSonido("Abyss BGM");
                GestorAudio.instancia.ReproducirSonido("Wrat Boss Fight");
            }
        }
        else if (nivel == 2)
        {
            if (!jefe)
            {
                GestorAudio.instancia.PausarSonido("Wrat Boss Fight");
                GestorAudio.instancia.PausarSonido("Vis Boss Fight");
                GestorAudio.instancia.ReproducirSonido("Tar Sea BGM");
            }

            if (jefe)
            {
                GestorAudio.instancia.PausarSonido("Tar Sea BGM");
                GestorAudio.instancia.ReproducirSonido("Vis Boss Fight");
            }
        }
        else if (nivel == 3)
        {
            if (!jefe)
            {
                GestorAudio.instancia.PausarSonido("Disparo Enemigo");
                GestorAudio.instancia.PausarSonido("Vis Boss Fight");
                GestorAudio.instancia.PausarSonido("Seraf Boss Fight");
                GestorAudio.instancia.ReproducirSonido("Dream Lab BGM");
            }

            if (jefe)
            {
                GestorAudio.instancia.PausarSonido("Dream Lab BGM");
                GestorAudio.instancia.ReproducirSonido("Seraf Boss Fight");
            }
        }
        else if (nivel == 4)
        {
            if (!jefe)
            {
                GestorAudio.instancia.PausarSonido("Seraf Boss Fight");
                GestorAudio.instancia.PausarSonido("Behoren Boss Fight");
                GestorAudio.instancia.ReproducirSonido("Defile BGM");
            }

            if (jefe)
            {
                GestorAudio.instancia.PausarSonido("Defile BGM");
                GestorAudio.instancia.ReproducirSonido("Behoren Boss Fight");
            }
        }
        else if (nivel == 5)
        {
            if (!jefe)
            {
                GestorAudio.instancia.PausarSonido("Behoren Boss Fight");
                GestorAudio.instancia.PausarSonido("Koning Boss Fight");
                GestorAudio.instancia.ReproducirSonido("Gold Castle BGM");
            }

            if (jefe)
            {
                GestorAudio.instancia.PausarSonido("Gold Castle BGM");
                GestorAudio.instancia.ReproducirSonido("Koning Boss Fight");
            }
        }
        else if (nivel == 6)
        {
            if (!jefe)
            {
                GestorAudio.instancia.PausarSonido("Koning Boss Fight");
                GestorAudio.instancia.PausarSonido("Heks Boss Fight");
                GestorAudio.instancia.ReproducirSonido("She BGM");
            }

            if (jefe)
            {
                GestorAudio.instancia.PausarSonido("She BGM");
                GestorAudio.instancia.ReproducirSonido("Heks Boss Fight");
            }
        }
        else if (nivel == 7 || nivel == 8)
        {
            GestorAudio.instancia.PausarSonido("Wrat Boss Fight");
            GestorAudio.instancia.PausarSonido("Vis Boss Fight");
            GestorAudio.instancia.PausarSonido("Seraf Boss Fight");
            GestorAudio.instancia.PausarSonido("Behoren Boss Fight");
            GestorAudio.instancia.PausarSonido("Koning Boss Fight");
            GestorAudio.instancia.PausarSonido("Heks Boss Fight");
        }
    }

    public void ReproducirSonidoSpawn(int enemigo)
    {
        if (enemigo == 1)
        {
            GestorAudio.instancia.ReproducirSonido("Aran Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 2)
        {
            GestorAudio.instancia.ReproducirSonido("Behoren Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 3)
        {
            GestorAudio.instancia.ReproducirSonido("Beukelaar Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 4)
        {
            GestorAudio.instancia.ReproducirSonido("Bloem Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 5)
        {
            GestorAudio.instancia.ReproducirSonido("Engel Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 6)
        {
            GestorAudio.instancia.ReproducirSonido("Geest Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 7)
        {
            GestorAudio.instancia.ReproducirSonido("Heks Spawn");
            numEnemigo = 0;

        }
        else if (enemigo == 8)
        {
            GestorAudio.instancia.ReproducirSonido("Helm Spawn");
            numEnemigo = 0;

        }
        else if (enemigo == 9)
        {
            GestorAudio.instancia.ReproducirSonido("Inktvis Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 10)
        {
            GestorAudio.instancia.ReproducirSonido("Koning Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 11)
        {
            GestorAudio.instancia.ReproducirSonido("Ruw Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 12)
        {
            GestorAudio.instancia.ReproducirSonido("Seraf Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 13)
        {
            GestorAudio.instancia.ReproducirSonido("Spellen Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 14)
        {
            GestorAudio.instancia.ReproducirSonido("Spin Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 15)
        {
            GestorAudio.instancia.ReproducirSonido("Tand Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 16)
        {
            GestorAudio.instancia.ReproducirSonido("Vis Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 17)
        {
            GestorAudio.instancia.ReproducirSonido("Welluw Spawn");
            numEnemigo = 0;
        }
        else if (enemigo == 18)
        {
            GestorAudio.instancia.ReproducirSonido("Wrat Spawn");
            numEnemigo = 0;
        }
    }

    public void ReproducirSonidoDisparo(int disparo)
    {
        if (disparo == 1)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Violeta");
        }
        else if (disparo == 2)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Cyan");
        }
        else if (disparo == 3)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Naranja");
        }
        else if (disparo == 4)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Rojo");
        }
        else if (disparo == 5)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Azul");
        }
        else if (disparo == 6)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Amarillo");
        }
        else if (disparo == 7)
        {
            GestorAudio.instancia.ReproducirSonido("Disparo Enemigo");
        }
    }

    private void ActivarMapa(int nivel)
    {
        if (nivel == 1)
        {
            RenderSettings.skybox = skyboxNivel3;
        }
        else if (nivel == 2)
        {
            RenderSettings.skybox = skyboxNivel2;
        }
        else if (nivel == 3)
        {
            RenderSettings.skybox = skyboxNivel3;
        }
    }

    private void ReiniciarNivel(int nivel)
    {
        jefe = false;
        noEntrar = true;
        spawn = false;
        final = false;
        ComenzarNivel(nivel);
    }
}