using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;
using UnityEngine.UI;

public class BarraUlti : MonoBehaviour
{
    public Image barraUlti;
    public float cantidadActualUlti;
    public float cantidadMaxUlti;

    void Update()
    {
        barraUlti.fillAmount = cantidadActualUlti / cantidadMaxUlti;
    }
}
