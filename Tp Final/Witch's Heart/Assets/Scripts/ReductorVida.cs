using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReductorVida : MonoBehaviour
{
    public float HP;
    public float HPPuntoDebil;
    public float HPAux;
    //public int puntosDestruidos;
    private ControlJugador reductorHP;
    private ControlJugador colorPro;
    //private ControlJugador energiaPJ;
    //private ControlJugador toque;
    [SerializeField] private string color;
    [SerializeField] private GameObject enemigo;
    [SerializeField] private GameObject enemigo_roto;
    [SerializeField] private GameObject puntoDebil;
    [SerializeField] private GameObject puntoDebil_roto;
    [SerializeField] private GameObject vida;
    private GameObject energia;
    private GameObject power;
    //public bool tocado = false;
    public int energiaOtorgada;
    //private bool noReducirContador = false;
    //private GameObject nada;
    //[SerializeField] private int cantidadPuntosDebiles;
    [SerializeField] private BarraDeVida barraDeVida;

    void Start()
    {
        reductorHP = FindObjectOfType<ControlJugador>();
        colorPro = FindObjectOfType<ControlJugador>();
        //barraDeVida = FindObjectOfType<BarraDeVida>();
        //energiaPJ = FindObjectOfType<ControlJugador>();
        //toque = FindObjectOfType<ControlJugador>();
        energia = GameObject.Find("Energia");
        HPAux = HP;
        power = GameObject.Find("PowerUp");
        if (gameObject.CompareTag("Core"))
        {
            barraDeVida.cantidadActual = HP;
            barraDeVida.cantidadMax = HP;
        }
        else if (gameObject.CompareTag("Weak"))
        {
            barraDeVida.cantidadActual = HPPuntoDebil;
            barraDeVida.cantidadMax = HPPuntoDebil;
        }
    }

    private void Update()
    {
        if (gameObject.CompareTag("Core"))
        {
        barraDeVida.cantidadActual = HP;
        }
        else if (gameObject.CompareTag("Weak"))
        {
            barraDeVida.cantidadActual = HPPuntoDebil;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet") && colorPro.colorPro == color)
        {
            recibirDaņo(1);
        }
        if (collision.gameObject.CompareTag("Player") && ControlJuego.instancia.HP.embistiendo)
        {
            recibirDaņo(2);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ulti"))
        {
            recibirDaņoUlti();
        }
    }

    public void recibirDaņo(int num)
    {
        if (num == 1)
        {
            if (gameObject.CompareTag("Core")/* && !ControlJuego.instancia.jefe*/)
            {
                HP -= reductorHP.daņo;
            }
            /*else if (gameObject.CompareTag("Boss") && ControlJuego.instancia.jefe && cantidadPuntosDebiles <= 0)
            {
                HP -= reductorHP.daņo;
            }*/
            else if (gameObject.CompareTag("Weak"))
            {
                HPPuntoDebil -= reductorHP.daņo;
            }
        }
        else if (num == 2)
        {
            if (gameObject.CompareTag("Core"))
            {
                HP = 0;
            }
        }

        if (HP <= 0 && gameObject.CompareTag("Core"))
        {
            /*if (ControlJuego.instancia.jefe)
            {
                ControlJuego.instancia.contTotalEnemigos--;
            }*/

            //noReducirContador = false;
            desaparecer();
        }
        /*if (HPPuntoDebil <= 0 && gameObject.CompareTag("Boss"))
        {
            noReducirContador = false;
            desaparecer();
        }*/
        if (HPPuntoDebil <= 0 && gameObject.CompareTag("Weak"))
        {
            //cantidadPuntosDebiles--;
            //noReducirContador = true;
            desaparecer();
        }
    }

    private void recibirDaņoUlti()
    {
        if (gameObject.CompareTag("Core") && !ControlJuego.instancia.jefe)
        {
            HP = 0;

            if (HP <= 0)
            {
                desaparecer();
            }
        }
    }

    public void desaparecer()
    {
        Destroy(gameObject);

        if (gameObject.CompareTag("Core"))
        {
            desarmar();
        }
        /*if (gameObject.CompareTag("Boss"))
        {
            desarmar();
        }*/
        else if (gameObject.CompareTag("Weak"))
        {
            //puntosDestruidos++;
            desarmar();
        }
    }

    public void desarmar()
    {
        if (gameObject.CompareTag("Core"))
        {
            enemigo_roto = Instantiate(enemigo_roto, transform.position, Quaternion.identity);
            Destroy(enemigo);

            if (!ControlJuego.instancia.jefe)
            {
                Destroy(enemigo_roto, 10);
            }

            if (Random.Range(0, 10) == 9)
            {
                crearVida();
            }

            if (Random.Range(0, 5) == 4)
            {
                crearPower();
            }

            if (Random.Range(0, 5) == 1)
            {
                crearEnergia();
            }
        }
        /*else if (gameObject.CompareTag("Boss"))
        {
            enemigo_roto = Instantiate(enemigo_roto, transform.position, Quaternion.identity);
            Destroy(enemigo);
            Destroy(enemigo_roto, 10);

            if (Random.Range(0, 10) == 9)
            {
                crearVida();
            }

            if (Random.Range(0, 5) == 4)
            {
                crearPower();
            }

            if (Random.Range(0, 2) == 1)
            {
                crearEnergia();
            }
        }*/
        else if (gameObject.CompareTag("Weak"))
        {
            Instantiate(puntoDebil_roto, transform.position, Quaternion.identity);
            Destroy(puntoDebil);

            if (Random.Range(0, 10) == 9)
            {
                crearVida();
            }

            if (Random.Range(0, 20) == 4)
            {
                crearPower();
            }
        }
    }

    private void crearVida()
    {
        Instantiate(vida, transform.position, Quaternion.identity);
    }

    private void crearPower()
    {
        Instantiate(power, transform.position, Quaternion.identity);
    }

    private void crearEnergia()
    {
        Instantiate(energia, transform.position, Quaternion.identity);
    }

    /*private IEnumerator Desactivar(int tipo, float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo--;
        }

        if (tipo == 1)
        {
            puntoDebil_roto.SetActive(false);
        }
        else if (tipo == 2)
        {
            enemigo_roto.SetActive(false);
        }
    }*/

    private void OnDestroy()
    {
        if (gameObject.CompareTag("Core")/* !noReducirContador && !ControlJuego.instancia.jefe*/)
        {
            ControlJuego.instancia.contTotalEnemigos -= 1;
        }

        if ((ControlJuego.instancia.HP.energia + energiaOtorgada) <= 100)
        {
            ControlJuego.instancia.HP.energia += energiaOtorgada;
        }

        if ((ControlJuego.instancia.HP.energia + energiaOtorgada) >= 100)
        {
            ControlJuego.instancia.HP.energia = 100;
        }
    }
}