using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMirarCamara : MonoBehaviour
{
    Vector2 mouseMirar; //a q angulo esta mirando la cam para los ejes x e y
    Vector2 suavidadV; //suaviza el moviemnto

    public float sensibilidad = 5.0f; //Cu�nto ten�s que mover el mouse para tener un movimiento del jugador
    public float suavizado = 2.0f; //Coeficiente de suavizado para la interpolaci�n lineal

    GameObject jugador;

    void Start()
    {
        jugador = this.transform.parent.gameObject; //Obtengo al Jugador por c�digo. Es el padre de la c�mara en este caso
    }

    void Update()
    {
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")); //Cu�nto cambia el movimiento desde el �ltimo update()

        md = Vector2.Scale(md, new Vector2(sensibilidad * suavizado, sensibilidad * suavizado));

        suavidadV.x = Mathf.Lerp(suavidadV.x, md.x, 1f / suavizado); //nterpolaci�n lineal: se obtienen valoresintermedios entre dos valores
        suavidadV.y = Mathf.Lerp(suavidadV.y, md.y, 1f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f); //Con Clamp hago que un valor est� dentro de un intervalo
        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right); //Rota la cam hacia arriba/abajo
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up); //Rota al jugador izq/der y por ende la c�mara
    }
}
