using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    public Image barraDeVida;
    public float cantidadActual;
    public float cantidadMax;

    void Update()
    {
        barraDeVida.fillAmount = cantidadActual/cantidadMax;
    }
}
