using UnityEngine;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;

public class DisparadorRapido : MonoBehaviour
{
    public GameObject proyectil;
    public int velocidad;
    public float tiempoRestante;
    private float tiempoAux;
    private GameObject pro;
    private float cont = 0;
    [SerializeField] private float tiempoEspera;

    void Start()
    {
        StartCoroutine(ComenzarCuenta(tiempoRestante));
    }

    void Update()
    {
        if (cont == tiempoRestante)
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(7);
            GameObject pro = Instantiate(proyectil, transform.position + transform.up, transform.rotation);
            pro.GetComponent<Rigidbody>().AddForce(transform.up * velocidad, ForceMode.Impulse);
            Destroy(pro, 5);
            cont = 0;
            StartCoroutine(ComenzarCuenta(tiempoRestante));
        }
    }

    private IEnumerator ComenzarCuenta(float valorCronometro)
    {
        tiempoAux = valorCronometro;

        while (tiempoAux > 0)
        {
            yield return new WaitForSeconds(tiempoEspera);
            tiempoAux -= tiempoEspera;
            cont += tiempoEspera;
        }
    }
}