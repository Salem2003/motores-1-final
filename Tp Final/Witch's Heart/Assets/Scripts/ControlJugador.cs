using System.Collections;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public static ControlJugador instancia;
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;
    public GameObject proyectilVioleta;
    public GameObject proyectilCyan;
    public GameObject proyectilNaranja;
    public GameObject proyectilRojo;
    public GameObject proyectilAzul;
    public GameObject proyectilAmarillo;
    public int da�o = 10;
    public float HP;
    public float HPAux;
    private Ray ray;
    public RaycastHit hit;
    private GameObject pro;
    private GameObject proAux;
    public string colorPro;
    private bool invencible;
    private float tiempoInvencibilidad = 2f;
    private bool disparo;
    private int cont = 0;
    private bool entrar = false;
    [SerializeField] private GameObject ulti;
    public int cooldownUlti; //120
    //private int cooldownDash = 0;
    private bool flag = true;
    private bool ultiLista = false;
    private bool enSuelo = true;
    private int saltoActual = 0;
    private int maxSaltos = 2;
    private int magnitudSalto = 10;
    private Rigidbody rb;
    //private ReductorVida tocado;
    //private ReductorVida HPEnemigo;
    //public bool desactivarToque = false;
    private float rapidezDash = 30f;
    public int energia = 0;
    public bool embistiendo = false;
    private bool reduciendo = false;
    public int ultiAux = 0;
    public bool instanciaJefe = false;
    public bool desactivarCorutinas = false;
    [SerializeField] private BarraDeVida barraDeVida;
    [SerializeField] private BarraUlti barraUlti;
    [SerializeField] private BarraDeEnergia barraDeEnergia;

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        //barraDeVida = FindObjectOfType<BarraDeVida>();
        barraDeVida.cantidadActual = HP;
        barraDeVida.cantidadMax = HP;
        barraUlti.cantidadActualUlti = ultiAux;
        barraUlti.cantidadMaxUlti = cooldownUlti;
        barraDeEnergia.cantidadActualEnergia = energia;
        barraDeEnergia.cantidadMaxEnergia = 100;
        HPAux = HP;
        Cursor.lockState = CursorLockMode.Locked;
        proAux = proyectilVioleta;
        colorPro = "violeta";
        rb = GetComponent<Rigidbody>();
        //tocado = FindObjectOfType<ReductorVida>();
        //HPEnemigo  = FindObjectOfType<ReductorVida>();
        ultiAux = cooldownUlti;
    }

    void Update()
    {
        barraDeVida.cantidadActual = HP;
        barraUlti.cantidadActualUlti = cooldownUlti;
        if (energia < 100)
        {
            barraDeEnergia.cantidadActualEnergia = energia;
        }

        /*if (ControlJuego.instancia.jefe && !desactivarCorutinas)
        {
            desactivarCorutinas = true;
            StopAllCoroutines();
        }*/

        if ((cooldownUlti - 1) < 0)
        {
            cooldownUlti = 0;
        }

        if (Input.GetKeyDown(KeyCode.Space) && (enSuelo || maxSaltos > saltoActual)) //este if es para el doble salto
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            enSuelo = false;
            saltoActual++;
        }

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * Time.deltaTime;
        float movimientoCostados = Input.GetAxis("Horizontal") * Time.deltaTime;

        if (!embistiendo)
        {
            movimientoAdelanteAtras *= rapidezDesplazamiento;
            movimientoCostados *= rapidezDesplazamiento;

            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        }
        else if (embistiendo)
        {
            reduciendo = false;

            movimientoAdelanteAtras *= rapidezDash;
            movimientoCostados *= rapidezDash;

            transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

            if (energia == 100 && !reduciendo)
            {
                Debug.Log(0 + 1);
                energia -= 1;
                reduciendo = true;
                StartCoroutine(ReducirEnergia(1));
            }

            if (energia <= 0)
            {
                energia = 0;
                embistiendo = false;
            }
        }

        /*if (energia <= 0)
        {
            embistiendo = false;
            energia = 0;
        }*/

        if (ControlJuego.instancia.jefe)
        {
            embistiendo = false;
        }

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetMouseButton(0) && !disparo && !embistiendo)
        {
            disparo = true;

            ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            if (!entrar)
            {
                StartCoroutine(Disparar(0.1f));
            }

            if (entrar)
            {
                StartCoroutine(DispararRapido(0.01f));
            }
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            proAux = proyectilVioleta;
            colorPro = "violeta";
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            proAux = proyectilCyan;
            colorPro = "cyan";
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            proAux = proyectilNaranja;
            colorPro = "naranja";
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            proAux = proyectilRojo;
            colorPro = "rojo";
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            proAux = proyectilAzul;
            colorPro = "azul";
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            proAux = proyectilAmarillo;
            colorPro = "amarillo";
        }

        if (HP <= 0)
        {
            HP = 500;
            energia = 0;
            cooldownUlti = ultiAux;
            ControlJuego.instancia.jefe = false;
            ControlJuego.instancia.indiceNivel = 1;
            ControlJuego.instancia.ComenzarNivel(1);
        }

        if (ControlJuego.instancia.guardarVida)
        {
            HPAux = HP;
        }

        if (ControlJuego.instancia.reinicio)
        {
            HP = HPAux;
            ControlJuego.instancia.reinicio = false;
        }

        if (cooldownUlti == ultiAux && flag && !ultiLista)
        {
            flag = false;
            StartCoroutine(CooldownUlti(1.0f));
        }

        if (Input.GetKeyDown(KeyCode.Q) && cooldownUlti <= 0 && !ControlJuego.instancia.jefe)
        {
            pro = Instantiate(ulti, transform.position, transform.rotation);
            //Rigidbody rb = ulti.GetComponent<Rigidbody>();
            //rb.AddForce(camaraPrimeraPersona.transform.forward * 500, ForceMode.Impulse);
            Destroy(pro, 20);
            ultiLista = false;
            cooldownUlti = ultiAux;
        }

        if (ControlJuego.instancia.jefe && !instanciaJefe)
        {
            //Destroy(pro);
            //Destroy(ulti);
            //StopAllCoroutines();
            embistiendo = false;
            invencible = false;
            instanciaJefe = true;
        }

        if (Input.GetKeyDown(KeyCode.E) && energia >= 100 && !ControlJuego.instancia.jefe)
        {
            embistiendo = true;
            //desactivarToque = false;
            invencible = true;
            //transform.Translate(rapidezDash * Vector3.forward * Time.deltaTime);
            //movimientoAdelanteAtras *= rapidezDash * Time.deltaTime;
            //movimientoCostados *= rapidezDash * Time.deltaTime;
            //transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        }

        //ulti.transform.localScale += new Vector3(5f, 5f, 5f) * Time.deltaTime;

        ControlJuego.instancia.SetearTextos(colorPro);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Floor") == true)
        {
            enSuelo = true;
            saltoActual = 0;
            //desactivarToque = true;
        }

        if (other.gameObject.CompareTag("Cure") == true && HP < 500)
        {
            if ((HP + 50) > 500)
            {
                HP = 500;
            }
            else
            {
                HP += 50;
            }
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Power") == true && cont == 0 && !embistiendo)
        {
            //StopCoroutine(DispararRapido());
            cont = 100;
            //entrar = false;
            other.gameObject.SetActive(false);
            StartCoroutine(DispararRapido(0.01f));

            entrar = true;
        }

        if (other.gameObject.CompareTag("Energy") == true && energia < 100)
        {
            if ((energia + 10) > 100)
            {
                energia = 100;
            }
            else
            {
                energia += 10;
            }
            other.gameObject.SetActive(false);
        }

        if (other.gameObject.CompareTag("Bad") && !embistiendo)
        {
            StartCoroutine(EsperarInvensibilidad(tiempoInvencibilidad));

            if (!invencible && tiempoInvencibilidad == 2f)
            {
                if (ControlJuego.instancia.jefe)
                {
                    HP -= 20;
                }
                else
                {
                    HP -= 10;
                }
            }

            invencible = true;
        }
        if (other.gameObject.CompareTag("Limit"))
        {
            if (ControlJuego.instancia.indiceNivel == 1)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(0f, 7f, 0f);
            }
            else if (ControlJuego.instancia.indiceNivel == 2)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(6903f, 7f, 2084f);
            }
            else if (ControlJuego.instancia.indiceNivel == 3)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(2586f, 7f, -1647f);
            }
            else if (ControlJuego.instancia.indiceNivel == 4)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(-3067f, 7f, -1647f);
            }
            else if (ControlJuego.instancia.indiceNivel == 5)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(254f, 7f, -2950f);
            }
            else if (ControlJuego.instancia.indiceNivel == 6)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(2516f, 7f, -2950f);
            }
            else if (ControlJuego.instancia.indiceNivel == 7)
            {
                ControlJuego.instancia.jugador.transform.position = new Vector3(2507f, 7f, -4211f);
            }
        }
        if (other.gameObject.CompareTag("Core") == true || other.gameObject.CompareTag("Weak") == true)
        {
            //transform.Translate(-rapidezDash * Vector3.forward * Time.deltaTime);
            //tocado.tocado = true;
            //HPEnemigo.HP -= 1000;
            StartCoroutine(EsperarInvensibilidad(tiempoInvencibilidad));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bad"))
        {
            StartCoroutine(EsperarInvensibilidad(tiempoInvencibilidad));

            if (!invencible && tiempoInvencibilidad == 2f)
            {
                if (ControlJuego.instancia.jefe)
                {
                    HP -= 20;
                }
                else
                {
                    HP -= 10;
                }
            }

            invencible = true;
        }
    }

    private IEnumerator EsperarInvensibilidad(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo -= 1.0f;
        }
        if (tiempo <= 0)
        {
            invencible = false;
            //tiempoInvencibilidad = 2f;
        }
    }

    private IEnumerator Disparar(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(0.01f);
            tiempo -= 0.01f;
        }

        pro = Instantiate(proAux, ray.origin, transform.rotation);
        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(camaraPrimeraPersona.transform.forward * 50, ForceMode.Impulse);
        Destroy(pro, 5);

        disparo = false;

        if (colorPro == "violeta")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(1);
        }
        else if (colorPro == "cyan")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(2);
        }
        else if (colorPro == "naranja")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(3);
        }
        else if (colorPro == "rojo")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(4);
        }
        else if (colorPro == "azul")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(5);
        }
        else if (colorPro == "amarillo")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(6);
        }
    }

    private IEnumerator DispararRapido(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(0.01f);
            tiempo -= 0.01f;
        }

        pro = Instantiate(proAux, ray.origin, transform.rotation);
        Rigidbody rb = pro.GetComponent<Rigidbody>();
        rb.AddForce(camaraPrimeraPersona.transform.forward * 50, ForceMode.Impulse);
        Destroy(pro, 5);

        disparo = false;

        if (colorPro == "violeta")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(1);
        }
        else if (colorPro == "cyan")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(2);
        }
        else if (colorPro == "naranja")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(3);
        }
        else if (colorPro == "rojo")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(4);
        }
        else if (colorPro == "azul")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(5);
        }
        else if (colorPro == "amarillo")
        {
            ControlJuego.instancia.ReproducirSonidoDisparo(6);
        }

        cont -= 1;

        if (cont == 0)
        {
            entrar = false;
        }
    }

    private IEnumerator CooldownUlti(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(1.0f);
            tiempo -= 1f;
            cooldownUlti--;
            ControlJuego.instancia.SetearTextos();
            if ((cooldownUlti - 1) < 0)
            {
                cooldownUlti = 0;
            }
        }

        if (cooldownUlti > 0)
        {
            StartCoroutine(CooldownUlti(1.0f));
        }

        if (cooldownUlti <= 0)
        {
            flag = true;
            ultiLista = true;
        }
    }

    private IEnumerator ReducirEnergia(float tiempo)
    {
        while (tiempo > 0)
        {
            yield return new WaitForSeconds(0.3f);
            tiempo -= 1f;
            cooldownUlti--;
            ControlJuego.instancia.SetearTextos();
        }

        if (energia > 0)
        {

            energia -= 1;
            StartCoroutine(ReducirEnergia(1));
        }
    }
}