using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using UnityEngine;

public class ControlMovEnemigo : MonoBehaviour
{
    public float rapidez;
    public float velocidadRotacion;
    private Transform objetivo = null;
    [SerializeField] private Rotador velRotY;

    void Start()
    {
        objetivo = GameObject.Find("Jugador").transform;
    }

    void Update()
    {
        Quaternion rotacion = Quaternion.LookRotation(objetivo.position - this.transform.position); //calculo el resultado de hasta donde debe rotar el objeto
        this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, rotacion, velocidadRotacion * Time.deltaTime); //roto lentamente al objeto en direccion a otro
        transform.Translate(rapidez * Vector3.forward * Time.deltaTime); //Avanza hacia adelante
    }
}
